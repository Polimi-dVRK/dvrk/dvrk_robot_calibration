//
// Created by luca18 on 7/26/17.
//

#include "calibration/calibration.hpp"

/// Constructor
Calibration::Calibration()
        : _image_rgb_sub(_node)
        , _image_grey_sub(_node)
        , _pnode("~")
{

    auto image_raw_topic = dvrk::getRequiredParam<std::string>(_pnode, "image_raw_topic");
    _image_rgb_sub.subscribe(image_raw_topic, &Calibration::onImageRGBReceived, this);

    auto image_grey_topic = dvrk::getRequiredParam<std::string>(_pnode, "image_grey_topic");
    _image_grey_sub.subscribe(image_grey_topic, &Calibration::onImageGreyReceived, this);

    auto PSM1_pose_topic = dvrk::getRequiredParam<std::string>(_pnode, "PSM1_pose_topic");
    _PSM1_pose_sub = _node.subscribe(PSM1_pose_topic, 1, &Calibration::onPSM1PoseReceived, this);

    auto PSM2_pose_topic = dvrk::getRequiredParam<std::string>(_pnode, "PSM2_pose_topic");
    _PSM2_pose_sub = _node.subscribe(PSM2_pose_topic, 1, &Calibration::onPSM2PoseReceived, this);

    auto pattern_size_str = dvrk::getRequiredParam<std::string>(_pnode, "pattern_size");
    auto pattern_size_parsed = dvrk::parsePoint(pattern_size_str);
    _pattern_size = *pattern_size_parsed;

    auto _home = dvrk::getRosHome();
    _path = *_home + "/DaVinci/";

    ROS_INFO_STREAM(
            "Calibration is ready, read the following configuration: \n"
                    << "    - Reading rgb images from: " << image_raw_topic.c_str() << "\n"
                    << "    - Reading grey images from: " << image_grey_topic.c_str() << "\n"
                    << "    - Reading PSM1 pose from: " << PSM1_pose_topic.c_str() << "\n"
                    << "    - Reading PSM2 pose from: " << PSM2_pose_topic.c_str() << "\n"
                    << "    - Pattern size (x,y): (" << pattern_size_str << ") \n"
                    << "    - Number of point that will be read: " << _number_of_points << "\n"
    );
}

/// Destructor
Calibration::~Calibration() {
    /// Do Nothing
}

void Calibration::onImageRGBReceived(const sensor_msgs::ImageConstPtr &img,
                                     const image_geometry::PinholeCameraModel &camera_model) {
    auto cv_image_rgb = cv_bridge::toCvCopy(*img, sensor_msgs::image_encodings::BGR8);
    _img_rgb = cv_image_rgb->image;
}

void Calibration::onImageGreyReceived(const sensor_msgs::ImageConstPtr &img,
                                      const image_geometry::PinholeCameraModel &camera_model) {
    auto cv_image_grey = cv_bridge::toCvCopy(*img);
    _img_grey = cv_image_grey->image;
    _dist_coeff = camera_model.distortionCoeffs();
    _camera_matrix = camera_model.intrinsicMatrix();
}

void Calibration::onPSM1PoseReceived(const geometry_msgs::PoseStampedPtr &pose) {
    _PSM1_pose = pose->pose;
}

void Calibration::onPSM2PoseReceived(const geometry_msgs::PoseStampedPtr &pose) {
    _PSM2_pose = pose->pose;
}


/**
 * This function is the "heart" of the calibration package. You can choose which kind of calibration to do.
 * The Hand-Eye calibration let you to compute the extrinsic parameters between the camera frame and PSM1 frame.
 * The Hand-Hand calibration let you to compute instead the extrinsic parameters between the PSM1 frame
 * and PSM2 frame. You can also proceed with both calibrations.
 */
void Calibration::calibration(Calibration::Type calib_type) {

    cv::namedWindow("Calibration", 1);

    // At first, find the chessboard corner locations
    find_chessboard_corners(image_points);

    if (calib_type == Type::HandToEye || calib_type == Type::All) {
        compute_ECM_PSM1_extrinsics();
    }

    if (calib_type == Type::HandToHand || calib_type == Type::All) {
        compute_PSM1_PSM2_extrinsics();
    }
}


/**
 * This function let you to find the chessboard corner with the Open_CV package "calib3d".
 *
 * @param image_points The chessboard corner positions on the image
 */
void Calibration::find_chessboard_corners(
        std::vector<cv::Point2d> &image_points
) {
    // Wait until we have received an image before finding the corner positions
    waitForImages();

    // We have received an image, find the chessboard and compute the image positions of the targets
    ROS_INFO("Acquiring chessboard corners...");

    size_t board_corners = _pattern_size.x * _pattern_size.y;
    std::vector<cv::Point2f> corners(board_corners);

    while (1) {
        ros::spinOnce();

        // Finds the positions of internal corners of the chessboard
        bool pattern_was_found = cv::findChessboardCorners(
                _img_grey, cv::Size(_pattern_size.x, _pattern_size.y), corners, CV_CALIB_CB_ADAPTIVE_THRESH|CV_CALIB_CB_FILTER_QUADS);

        // Refines the corner locations
        if (pattern_was_found && corners.size() == board_corners) {
            cv::cornerSubPix(_img_grey, corners, cv::Size(3, 3), cv::Size(-1, -1),
                             cv::TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 10, 1E-4));

        image_points = {corners[_pattern_size.x + 1], corners[(_pattern_size.y - 2) * _pattern_size.x + 1],
                        corners[_pattern_size.x + 3], corners[(_pattern_size.y - 2) * _pattern_size.x + 3],
                        corners[_pattern_size.x + 5], corners[(_pattern_size.y - 2) * _pattern_size.x + 5],
                        corners[_pattern_size.x + 7], corners[(_pattern_size.y - 2) * _pattern_size.x + 7]};
            break;
        }

        cv::imshow("Calibration", _img_rgb);
        cv::waitKey(25);
    }

    ROS_INFO("Chessboard corners acquired!");
}


/**
 * This function let you to acquire the tip position of PSM1/PSM2 arm in proximity of the chessboard corners.
 *
 * @param object_points The acquired tip positions
 * @param image_points The chessboard corner positions on the imahe
 * @param arm_selection The arm used to acquire the points (PSM1 or PSM2)
 */
void Calibration::acquire_arm_points(
        std::vector<cv::Point3d> &object_points,
        std::vector<cv::Point2d> image_points,
        std::string arm_selection
) {
    // Save 3D points with the robot tip
    ROS_INFO_STREAM("Acquire "<< arm_selection << " points pressing 'l'");

    int counter = 0;

    while(1) {
        ros::spinOnce();

        // Draw the target points. The next point to be targeted is colored red
        for (int i=0; i < _number_of_points; i++) {
            auto target_color = (i == counter ? CV_RGB(255, 0, 0) : CV_RGB(0 , 255, 0));
            circle(_img_rgb, image_points[i], 10, target_color, 2);
        }

        // Display the image
        cv::imshow("Calibration", _img_rgb);

        int c = cv::waitKey(15);
        if (c == 'l') {

            cv::Point3d position;

            if (arm_selection == "PSM1") {
                position = dvrk::toPoint3d(_PSM1_pose.position);

            } else if (arm_selection == "PSM2") {
                position = dvrk::toPoint3d(_PSM2_pose.position);
            }

            object_points.push_back(position);

            ROS_INFO_STREAM(
                    "Acquired " << arm_selection << " point (x, y, z) = (" << position.x << ", "
                                << position.y << ", " << position.z << ")"
            );

            counter += 1;
            if (counter == _number_of_points) {
                break;
            }
        }

        cv::waitKey(5);
    }

    ROS_INFO_STREAM(arm_selection << " points saved!");
}


/**
 * Compute the camera-PSM1 extrinsic parameters and save them in a xml file.
 */
void Calibration::compute_ECM_PSM1_extrinsics() {

    acquire_arm_points(PSM1_points, image_points, "PSM2");

    ROS_INFO("Computing ECM-PSM1 extrinsic calibration ...");

    cv::Mat R;

    // Finds an object pose from 3D-2D point correspondences
    cv::solvePnP(PSM1_points, image_points, _camera_matrix, _dist_coeff, R, t_ECM_PSM1);

    // Converts a rotation vector to a rotation matrix
    cv::Rodrigues(R, R_ECM_PSM1);

    ROS_INFO("Done computing ECM-PSM1 extrinsics!");
    ROS_INFO_STREAM("Rotation " << R_ECM_PSM1);
    ROS_INFO_STREAM("Translation " << t_ECM_PSM1);

    // Save Extrinsic calibration
    saveExtrinsicParams(_path + "ECM_PSM1_Extrinsics.xml", R_ECM_PSM1, t_ECM_PSM1);
}


/**
 * Compute the PSM1-PSM2 extrinisc parameters and save them in a xml file.
 */
void Calibration::compute_PSM1_PSM2_extrinsics() {

    acquire_arm_points(PSM2_points, image_points, "PSM2");

    // Convert acquired points to std::vector<Eigen::Vector3d>
    cv2eigen(eigen_PSM1_points, PSM1_points);
    cv2eigen(eigen_PSM2_points, PSM2_points);

    ROS_INFO("Computing PSM1-PSM2 extrinsic calibration ...");

    // Convert std::pair <Eigen::Matrix3d, Eigen::Vector3d> to cv::Mat
    eigen2mat(R_PSM1_PSM2, t_PSM1_PSM2, computeRigidTransform(eigen_PSM1_points, eigen_PSM2_points));

    ROS_INFO("Done computing PSM1-PSM2 extrinsics!");
    ROS_INFO_STREAM("Rotation " << R_PSM1_PSM2);
    ROS_INFO_STREAM("Translation " << t_PSM1_PSM2);

    // Save Extrinsic calibration
    saveExtrinsicParams(_path + "PSM1_PSM2_Extrinsics.xml", R_PSM1_PSM2, t_PSM1_PSM2);
}


/**
 * Compute the rigid transformation between two sets of points.
 */
std::pair <Eigen::Matrix3d, Eigen::Vector3d> Calibration::computeRigidTransform(
        const std::vector <Eigen::Vector3d> &src, const std::vector <Eigen::Vector3d> &dst)
{
    assert(src.size() == dst.size());
    int pairSize = static_cast<int> (src.size());
    Eigen::Vector3d center_src(0, 0, 0), center_dst(0, 0, 0);
    for (int i = 0; i < pairSize; ++i) {
        center_src += src[i];
        center_dst += dst[i];
    }
    center_src /= (double) pairSize;
    center_dst /= (double) pairSize;

    Eigen::MatrixXd S(pairSize, 3), D(pairSize, 3);
    for (int i = 0; i < pairSize; ++i) {
        for (int j = 0; j < 3; ++j)
            S(i, j) = src[i][j] - center_src[j];
        for (int j = 0; j < 3; ++j)
            D(i, j) = dst[i][j] - center_dst[j];
    }
    Eigen::MatrixXd Dt = D.transpose();
    Eigen::Matrix3d H = Dt * S;
    Eigen::Matrix3d W, U, V;

    Eigen::JacobiSVD <Eigen::MatrixXd> svd;
    Eigen::MatrixXd H_(3, 3);
    for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) H_(i, j) = H(i, j);
    svd.compute(H_, Eigen::ComputeThinU | Eigen::ComputeThinV);
    if (!svd.computeU() || !svd.computeV()) {
        ROS_ERROR("Decomposition error");
        return std::make_pair(Eigen::Matrix3d::Identity(), Eigen::Vector3d::Zero());
    }
    Eigen::Matrix3d Vt = svd.matrixV().transpose();
    Eigen::Matrix3d R = svd.matrixU() * Vt;
    Eigen::Vector3d t = center_dst - R * center_src;

    return std::make_pair(R, t);
}


/**
 *  Wait for RGB and grey images. If one of these two images is not received, we shutdown the program.
 */
void Calibration::waitForImages() {

    bool _img_grey_received = true;
    bool _img_rgb_received = true;

    ROS_INFO("Waiting for images...");

    if (!_image_grey_sub.waitForFirstImage(5.0)) {
        _img_grey_received = false;
        ROS_ERROR("Timeout whilst waiting for images on <%s>.", _image_grey_sub.getTopic().c_str());
    } else if (!_image_rgb_sub.waitForFirstImage(5.0)) {
        _img_rgb_received = false;
        ROS_ERROR("Timeout whilst waiting for images on <%s>.", _image_rgb_sub.getTopic().c_str());
    }

    if (!_img_grey_received || !_img_rgb_received) {
        ROS_ERROR("Exiting...");
        ros::shutdown();
        exit(-1);
    } else {
        ROS_INFO("RGB and grey images received!");
    }
}


/**
 * Save the extrinsic parameters.
 */
void Calibration::saveExtrinsicParams(
        const std::string filePath,
        cv::Mat &R,
        cv::Mat &T)
{
    const fs::path output_dir(filePath);
    const fs::path dirPath = output_dir.parent_path();
    const fs::path fileName = output_dir.filename();

    if (!fs::exists(dirPath)) {
        ROS_WARN_STREAM("Destination path \"" << dirPath << "\" does not exist. Creating directory...");
        if (!fs::create_directories(dirPath)) {
            ROS_ERROR_STREAM("Unable to create directory \"" << dirPath << "\". Results will not be saved.");
        };
    }

    // Saving all parameters
    cv::FileStorage fs(filePath, cv::FileStorage::WRITE);
    if (!fs.isOpened()) {
        ROS_ERROR_STREAM("Unable to open file " << fileName);
    } else {
        ROS_INFO_STREAM(fileName << " saved in " << dirPath << " directory");
    }

    fs << "R" << R;
    fs << "T" << T;
    fs.release();
}


/**
 * Convert std::vector<cv::Point3f> to std::vector<Eigen::Vector3d>.
 */
void Calibration::cv2eigen(
        std::vector<Eigen::Vector3d> &eigen_points,
        std::vector<cv::Point3d> cv_points
) {
    for (int i = 0; i < cv_points.size(); i++) {
        eigen_points.emplace_back(
                cv_points[i].x, cv_points[i].y, cv_points[i].z);
    }
}


/**
 * Convert std::pair <Eigen::Matrix3d, Eigen::Vector3d> to cv::Mat.
 */
void Calibration::eigen2mat(
        cv::Mat &R,
        cv::Mat &t,
        std::pair<Eigen::Matrix3d, Eigen::Vector3d> T
) {
    R.create(3, 3, CV_64FC1);
    R.at<double>(0, 0) = T.first.data()[0];
    R.at<double>(1, 0) = T.first.data()[1];
    R.at<double>(2, 0) = T.first.data()[2];
    R.at<double>(0, 1) = T.first.data()[3];
    R.at<double>(1, 1) = T.first.data()[4];
    R.at<double>(2, 1) = T.first.data()[5];
    R.at<double>(0, 2) = T.first.data()[6];
    R.at<double>(1, 2) = T.first.data()[7];
    R.at<double>(2, 2) = T.first.data()[8];

    t.create(3, 1, CV_64FC1);
    t.at<double>(0, 0) = T.second.x();
    t.at<double>(1, 0) = T.second.y();
    t.at<double>(2, 0) = T.second.z();
}