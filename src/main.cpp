//
// Created by luca18 on 7/26/17.
//

#include "calibration/calibration.hpp"

int main(int argc, char **argv)
{
	ros::init(argc, argv, "calibration");

    cv::startWindowThread();

    // Definition of Class
    Calibration Calib;

    Calibration::Type calibration_type=Calibration::Type::All;

    for (int i =1; i < argc; i++) {

        if (std::string(argv[i]) == "hand-hand") {

            calibration_type = Calibration::Type::HandToHand;
            ROS_INFO("Hand-Hand calibration selected");

        } else if (std::string(argv[i]) == "hand-eye") {

            calibration_type = Calibration::Type::HandToEye;
            ROS_INFO("Hand-Eye calibration selected");

        } else {

            ROS_INFO("Hand-Hand & Hand-Eye calibrations selected");

        }

    }

    Calib.calibration(calibration_type);

    return 0;
}
