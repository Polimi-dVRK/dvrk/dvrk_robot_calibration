//
// Created by luca18 on 7/26/17.
//

#pragma once

// Boost
#include <boost/filesystem.hpp>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// KDL
#include <kdl_conversions/kdl_msg.h>

// ROS
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <geometry_msgs/PoseStamped.h>
#include <eigen3/Eigen/Geometry>
#include <cv_bridge/cv_bridge.h>

// dvrk Library
#include "dvrk_common/ros/params.hpp"
#include "dvrk_common/ros/util.hpp"
#include "dvrk_common/parse.hpp"
#include "dvrk_common/camera/simple_camera.hpp"
#include "dvrk_common/conversions/cv_ros.hpp"

namespace fs = boost::filesystem;

class Calibration {

public: /* Types */
    enum class Type {
        All, HandToHand, HandToEye
    };

public:

    /* Constructor */
    Calibration();
    /* Destructor */
    virtual ~Calibration();

    /* Methods */
    void calibration(Calibration::Type calib_type = Type::All);

    /* ROS Callbacks */
    void onImageRGBReceived(const sensor_msgs::ImageConstPtr &rgb_l,
                            const image_geometry::PinholeCameraModel &camera_model);
    void onImageGreyReceived(const sensor_msgs::ImageConstPtr &rgb_l,
                             const image_geometry::PinholeCameraModel &camera_model);
    void onPSM1PoseReceived(const geometry_msgs::PoseStampedPtr &pose);
    void onPSM2PoseReceived(const geometry_msgs::PoseStampedPtr &pose);

private:

    /* Methods */
    void find_chessboard_corners(
            std::vector<cv::Point2d> &image_points);

    void acquire_arm_points(
            std::vector<cv::Point3d> &object_points,
            std::vector<cv::Point2d> image_points,
            std::string arm_selection);

    void compute_ECM_PSM1_extrinsics();
    void compute_PSM1_PSM2_extrinsics();

    void saveExtrinsicParams(
            const std::string filePath,
            cv::Mat &R,
            cv::Mat &T);

    void waitForImages();

    std::pair <Eigen::Matrix3d, Eigen::Vector3d> computeRigidTransform(
            const std::vector <Eigen::Vector3d> &src, const std::vector <Eigen::Vector3d> &dst);

    /* Conversion functions */
    void cv2eigen(
            std::vector<Eigen::Vector3d> &eigen_points,
            std::vector<cv::Point3d> cv_points);

    void eigen2mat(
            cv::Mat &R,
            cv::Mat &t,
            std::pair<Eigen::Matrix3d, Eigen::Vector3d> T);

    /* ROS Parameters */
    ros::NodeHandle _node;
    ros::NodeHandle _pnode;

    ros::Subscriber _PSM1_pose_sub;
    ros::Subscriber _PSM2_pose_sub;

    dvrk::SimpleCamera _image_rgb_sub;
    dvrk::SimpleCamera _image_grey_sub;

    geometry_msgs::Pose _PSM1_pose;
    geometry_msgs::Pose _PSM2_pose;

    /* Params */
    cv::Point _pattern_size;

    /* Member Variables */
    cv::Mat _img_rgb;
    cv::Mat _img_grey;

    cv::Mat _dist_coeff;
    cv::Matx33d _camera_matrix;

    unsigned int _number_of_points = 8;

    std::string _path;

    /* Variables */
    std::vector<cv::Point2d> image_points; // Points to target with the robot
    std::vector<cv::Point3d> PSM1_points; // PSM1 position acquired
    std::vector<cv::Point3d> PSM2_points; // PSM2 position acquired

    std::vector <Eigen::Vector3d> eigen_PSM1_points, eigen_PSM2_points;

    cv::Mat R_ECM_PSM1, t_ECM_PSM1; // Rotation matrix and translation vector
    cv::Mat R_PSM1_PSM2, t_PSM1_PSM2;
};
